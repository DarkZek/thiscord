const Vuex = require('vuex');

import { GuildMutations, GuildActions } from '../services/GuildService/Store/GuildStore.store';
import { UserMutations } from '../services/UserService/Store/UserStore.store';

class VuexStoreService {
  Generate() {
    const store = new Vuex.Store({
      state: {
        user: {
          loggedIn: false
        }, 
        guilds: {}, 
        settings: {},
        selectedGuild: {
          channel: {},
          guild: {},
          messages: [],
          members: {}
        },
        invites: {},
        users: {}
      },
      mutations: Object.assign({}, GuildMutations, UserMutations),
      actions: Object.assign({}, GuildActions)
    });

    return store;
  }
}

export default new VuexStoreService();