/*
    This class is the jumpoff point to load Vue.js and start the services
*/

window.Vue = require("vue");
const Vuex = require('vuex');

Vue.component(
    'app',
    require("./App.vue").default
);

Vue.use(Vuex);
window.events = new Vue();

import VuexStore from './VuexStore.js';
var store = VuexStore.Generate();

//Helper functions
import '@/app/core/services/HelperService/HelperService.service.js';
import AppManager from '@/app/core/loader/AppManager.js';

$(document).ready(function () {
    window.store = store;

    var appManager = new AppManager(store);
    
    appManager.Start(store, function() {
        const app = new Vue({
            el: '#app',
            store
        });
    });
});