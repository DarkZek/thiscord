//Include all services
import ChannelService from '@/app/core/services/ChannelService/ChannelService.service.js';
import GuildService from '@/app/core/services/GuildService/GuildService.service.js';
import MessageService from '@/app/core/services/MessageService/MessageService.service.js';
import ModalService from '@/app/core/services/ModalService/ModalService.service.js';
import UserService from '@/app/core/services/UserService/UserService.service.js';
import WebsocketService from '@/app/core/services/WebsocketService/WebsocketService.service.js';

class AppManager {

    Start(store, cb) {

        this.services = {
            ChannelService,
            GuildService,
            MessageService,
            ModalService,
            UserService,
            WebsocketService
        };

        this.store = store;
        this.store.state.app = this;
        this.events = window.events;
        this.token = $.cookie('token');
        //TODO: Remove
        window.appManager = this;
        window.settings.token = this.token;

        console.log("Connecting to API service...");
        UserService.CheckAuthentication(function(loggedIn, user) {
            
            if (!loggedIn) {

                //Check if cookies are disabled
                if (!navigator.cookieEnabled) {
                    alert("Cookies are disabled, please enable them to use Thiscord");
                    return;
                }
    
                $(".loader").addClass("disableLoader");
                cb();
                return;
            }
            this.Initialize();

            console.log("Logging in as " + user.name);
            this.store.commit('login', user);
            
            GuildService.LoadGuilds(function() {
                var guild = Object.keys(this.store.state.guilds)[0];

                //Get guild user was last in
                if (this.store.state.guilds[$.cookie("last_guild")]) {
                    guild = $.cookie("last_guild");
                }

                //If user is in no guilds return
                if (!guild) {
                    this.store.commit('setLoggedIn', true);
                    $(".loader").addClass("disableLoader");
                    this.Initialize();
                    if (cb) {cb();}
                    return;
                }

                console.log("Loading guild " + guild);
                //Load guild channels
                this.store.dispatch('selectGuild', {guild, cb: function() {
                    UserService.SetupListeners();

                    this.store.commit('setLoggedIn', true);
                    if (cb) {cb();}

                    $("*[channel]").click(function(click) {
                        ChannelService.LoadChannel(this.store.state.selectedGuild.guild._id, click.currentTarget.getAttribute('channel'))
                    }.bind(this));

                    $("*[guild]").click(function(click) {
                        this.store.dispatch('selectGuild', { guild: click.currentTarget.getAttribute('guild') })
                    }.bind(this));

                    //Hide loader
                    $(".loader").addClass("disableLoader");
                }.bind(this)});
            }.bind(this));
        }.bind(this));
    }

    Initialize() {
        this.socket = WebsocketService.Initialize(this.store, this.token);
        window.socket = this.socket;
        UserService.Initialize(this);
        ModalService.Initialize(this);
        GuildService.Initialize(this);
        MessageService.Initialize(this);
        ChannelService.Initialize(this);
    }
}

export default AppManager;