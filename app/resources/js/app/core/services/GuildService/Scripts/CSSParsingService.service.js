/*
Author: https://github.com/SimplyJpk
*/

class CSSParsingService {
    // So you'd call ``parseCss(WhateverCss, ".chat"``
    parseCss(css, lead) 
    {
        var ReturnedString = "";
        // This should match all of a CSS 'chunk'
        var RegexResult = css.split(/.+\\{[^}]+\\}/);
        if (RegexResult !== null && RegexResult.length > 0)
        {
            RegexResult.forEach(function (Str)
            {
                if (Str.length <= 0) // Rare
                {
                    return;
                }
                ReturnedString += lead + " " + Str;
            });
        }
        if (ReturnedString == "")
            return css;
        return ReturnedString;
    }
}

export default new CSSParsingService();