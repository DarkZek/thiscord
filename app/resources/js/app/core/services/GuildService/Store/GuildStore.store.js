module.exports.GuildMutations = {
    updateGuild(state, guild) {
        Vue.set(state.guilds, guild._id, guild);
    },

    removeGuild(state, guild) {
        Vue.delete(state.guilds, guild);
    },
    
    setSelectedGuild(state, guildId) {
        state.selectedGuild.guild = state.guilds[guildId];
        state.selectedGuild.members = state.selectedGuild.members;
    },

    updateInvite(state, guild) {
        state.invites[guild.invite] = guild;
    }
};

module.exports.GuildActions = {

    /* Requires { guildId: id of the guild, cb: callback} */
    updateGuild(context, settings) {
        if (context.state.guilds[settings.guildId].channels) {
            return settings.cb(context.state.guilds[settings.guildId]);
        }

        $.GET('guild/' + settings.guildId, function(data, result) {
            context.commit('updateGuild', result.responseJSON);
            settings.cb();
        }.bind(context));
    },

    removeGuild(context, guildId) {
        context.commit('removeGuild', guildId);
        context.commit('setSelectedGuild', context.store.selectedGuild[0]._id);
    },

    /* Requires { invite: String, cb: callback} */
    getInviteInfo(context, settings) {

        //Already cached
        const invite = context.state.invites[settings.invite];
        if (invite && invite.name) {
            return settings.cb(context.state.invites[settings.invite]);
        }

        //Theres a queue for the data
        if (invite && invite.queue) {
            var queue = invite.queue;
            queue.push(settings.cb);
            return context.commit('updateInvite', {invite: settings.invite, queue})
        }

        context.commit('updateInvite', {invite: settings.invite, queue: [ settings.cb ]});

        //Theres no queue and its not cached, lets fetch it
        $.GET("guild/invite?invite=" + settings.invite, function(response, data) {
            if (response != "success") {
                return window.log({type: "error", text: "Failed getting invite info"});
            }

            var queue = context.state.invites[settings.invite].queue;
            const inviteInfo = data.responseJSON.guild;
            inviteInfo.invite = settings.invite;

            queue.forEach(consumer => {
                consumer(inviteInfo);
            });

            context.commit('updateInvite', inviteInfo);
        });
    },

    /* Requires { guild: id of the guild, cb: callback} */
    selectGuild(context, settings) {
        //Update guild css
        context.dispatch("updateGuild", {
            guildId: settings.guild,
            cb: function(result) {
                context.commit('setSelectedGuild', settings.guild); 
                context.state.app.services['GuildService'].UpdateGuildCss();
                
                //After loaded guild
                //Get all users in guild
                context.state.app.services['UserService'].GetUsers(settings.guild, function() {
                    //Channel to load
                    const channel = Object.entries(context.state.selectedGuild.guild.channels)[0][1]._id;

                    context.state.app.services['ChannelService'].SelectChannel(settings.guild, channel, settings.cb);
        
                }.bind(this));
            }.bind(this)
        });

        $.cookie("last_guild", settings.guild);
    }
};