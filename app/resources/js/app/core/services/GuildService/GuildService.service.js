import CSSParser from "./Scripts/CSSParsingService.service.js";

class GuildService {
    Initialize(appManager) {
        this.store = appManager.store;
        this.events = appManager.events;
        this.socket = appManager.socket;
        this.channelService = appManager.services.ChannelService;
        this.userService = appManager.services.UserService;
        this.SetupListeners();
    }

    LoadGuilds(cb) {
        $.GET(
            "user/guilds/list",
            function(res, data) {
                data.responseJSON.forEach(guild => {
                    window.store.commit("updateGuild", guild);
                });
                cb();
            }.bind(this)
        );
    }

    UpdateGuildCss(guild) {
        const css = this.store.state.selectedGuild.guild.css;

        $("#guild-css")[0].innerHTML = CSSParser.parseCss(css, ".editable");
    }

    SetupListeners() {
        this.events.$on("guild-create", function(data) {
            $.POST("guild/create", data, function(result, data) {
                if (result != "success") {
                    alert(data.error.message);
                } else {
                    window.store.commit("updateGuild", data.responseJSON.guild);
                }
            });
        });

        this.events.$on(
            "update-guild",
            function(data) {
                this.SaveSettings(data);
            }.bind(this)
        );

        this.events.$on(
            "fetch-invite",
            function(data) {
                this.FetchInviteInfo(data.invite, data.cb);
            }.bind(this)
        );

        this.socket.on('user-update', (data) => {
            this.store.commit('userUpdate', data);
        });
    }

    SaveSettings(data) {
        
        if (data.brand.icon) {
            this.UpdateIcon(data.brand.icon);
            data.brand.icon = null;
        }
        
        if (data.brand.background) {
            this.UpdateBackground(data.brand.background);
            data.brand.background = null;
        }

        $.PATCH(
            "guild/" + this.store.state.selectedGuild.guild._id + "/",
            data
        );
    }

    UpdateIcon(icon) {
        var formdata = new FormData();

        formdata.append("icon", icon);

        $.PATCH(
            "guild/" + this.store.state.selectedGuild.guild._id + "/icon",
            formdata
        );
    }

    UpdateBackground(icon) {
        var formdata = new FormData();

        formdata.append("icon", icon);

        $.PATCH(
            "guild/" + this.store.state.selectedGuild.guild._id + "/background",
            formdata
        );
    }

    FetchInviteInfo(invite, cb) {
    }
}

export default new GuildService();
