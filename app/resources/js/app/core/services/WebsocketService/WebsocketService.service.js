
var io = require('socket.io-client');

class WebsocketService {

    Initialize(store, token) {
        this.store = store;
        this.token = token;
        return this.SetupListeners();
    }

    SetupListeners() {

        //Setup listeners to update pages
        var socket = io.connect(window.settings.api);

        socket.on('connect', () => {
            $(".loader").addClass("disableLoader");
        });

        socket.emit('auth', {
            token: this.token
        });

        socket.emit('join', {
            channel: store.state.selectedGuild.channel._id
        });

        socket.on('disconnect', (reason) => {
            $(".loader").removeClass("disableLoader");
        });

        return socket;
    }
}

export default new WebsocketService();