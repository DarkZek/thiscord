//Cookie management
$.cookie = function(cname, cvalue) {
    //It just wants the data of the cookie
    if (!cvalue) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for(var i = 0; i <ca.length; i++) {
          var c = ca[i];
          while (c.charAt(0) == ' ') {
            c = c.substring(1);
          }
          if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
          }
        }
        return "";
    } else {
        //Set the cookie
        var d = new Date();
        d.setTime(d.getTime() + (100*24*60*60*1000));
        var expires = "expires="+ d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }
}

//GET requests with authorization
$.GET = function(url, cb) {
  $.ajax({
    url: window.settings.api + url,
    type: "GET",
    headers: {"Authorization": 'Bearer ' + window.settings.token},
    complete: function(data, status) {
      if (cb) {cb(status, data);}
    }  
  });
};

//POST requests with authorization
$.POST = function(url, data, cb) {
  
  var req = {
    url: window.settings.api + url,
    type: "POST",
    data: data,
    headers: {"Authorization": 'Bearer ' + window.settings.token},
    complete: function(data, status) {
      if (cb) {cb(status, data); }
    }  
  };

  try {
    if (data instanceof FormData) {
      req.processData = false;
      req.contentType = false;
    }
  } catch(err) {}

  $.ajax(req);
};

//POST requests with authorization
$.PATCH = function(url, data, cb) {
  
  var req = {
    url: window.settings.api + url,
    type: "PATCH",
    data: data,
    headers: {"Authorization": 'Bearer ' + window.settings.token},
    complete: function(data, status) {
      if (cb) {cb(status, data); }
    }  
  };

  try {
    if (data instanceof FormData) {
      req.processData = false;
      req.contentType = false;
    }
  } catch(err) {}

  $.ajax(req);
};

//Previews images 
$.PREVIEW = function(inputSelector, placeholderSelector) {
  const input = $(inputSelector)[0];
  if (input.files && input.files[0]) {
      var reader = new FileReader();
      
      reader.onload = function (e) {
        console.log($(placeholderSelector));
        $(placeholderSelector).attr('src', e.target.result);
      }
      
      reader.readAsDataURL(input.files[0]);
  } else {
      alert("Your browser does not support file previews, so none will show");
  }
}

$.COPY = function(text) {
  navigator.clipboard.writeText(text);
};