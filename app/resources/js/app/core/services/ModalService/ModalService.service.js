class ModalService {

    Initialize() {
        this.modals = [];
        this.displaying = false;
        window.modal = this;

        //Listen for keypresses
        $('body').keydown(function(e){
            if(e.key == "Escape"){
                // Close my modal window
                this.DiscardTopModal();
            }
        }.bind(this));        
    }

    RegisterModal(component, ref) {
        this.modals.push({component: component, ref: ref});

        this.Update();
    }

    DiscardTopModal() {

        if (this.modals.length == 0) {
            return;
        }

        var modal = this.modals[this.modals.length - 1];
        const resistDiscard = modal.component.Discard();
        
        //Congratulations, you are being rescued. Please do not resist.
        if (resistDiscard) {
            //Modal wants to stay open
            return;
        }

        //Modal is ok to close
        this.modals.pop();
        this.Update();
    }

    Update() {

        //If theres no popups and its not displaying
        if (this.modals.length == 0 && !this.displaying) {
            return;
        }

        //If there are no popups but its displaying
        if (this.modals.length == 0 && this.displaying) {
            //Hide background
            var blackout = $(".blackout")[0];
            blackout.style.opacity = '0';
            setTimeout(function() {
                blackout.style.display = 'none';
            }, 100);

            this.displaying = false;
            return;
        }

        if (this.modals.length > 0 && !this.displaying) {
            var blackout = $(".blackout")[0];
            blackout.style.display = 'block';
    
            //Get fade to display
            setTimeout(function() {
                blackout.style.opacity = '1';
            }, 1);
    
            $(".blackout").click(function() {
                this.DiscardTopModal();
            }.bind(this));

            this.displaying = true;
        }

        //Update z-index's
        for(var z = 0; z < this.modals.length; z++) {
            var modal = this.modals[z];

            //Climbs in 10's to give breathing room between
            modal.ref.css('z-index', (z + 1) * 10);
        }
        
        //Set blackouts z-index
        $(".blackout").css('z-index', ((this.modals.length) * 10) - 1);
    }
    
}

export default new ModalService();