import { runInThisContext } from "vm";

class ChannelService {

    Initialize(appManager) {
        this.store = appManager.store;
        this.socket = appManager.socket;
        this.messageService = appManager.services.MessageService;
    }

    SelectChannel(guild, channel, cb) {
        store.commit('setSelectedChannel', {guildId: guild, channelId: channel});

        this.socket.emit('join', {
            channel: store.state.selectedGuild.channel._id
        });

        //Fetch all messages in selected channel
        this.messageService.GetMessages(guild, channel,  function() {
            if (cb) {cb();}
        });
    }
}

export default new ChannelService();