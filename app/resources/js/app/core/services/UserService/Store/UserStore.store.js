module.exports.UserMutations = {
    login (state, user) {
        if (!user.loggedIn) {
            user.loggedIn = false;
        }
        state.user = user;
    },
    //Seperated in order to make Vue only show guilds and such once we have loaded all guilds 
    setLoggedIn (state, loggedIn) {
        state.user.loggedIn = loggedIn;
    },
    updateMessages(state, update) {
        state.guilds[update.guildId].channels[update.channelId].messages = update.messages;
    },
    addMessage(state, update) {
        state.selectedGuild.messages.push(update.message);
    },
    setDisplayedMessages(state, update) {
        state.selectedGuild.messages = state.guilds[update.guildId].channels[update.channelId].messages;
    },
    setSelectedChannel(state, update) {
        state.selectedGuild.channel = state.guilds[update.guildId].channels[update.channelId];
    },
    setPresence(state, update) {
        if (!state.users[update._id]) {
            state.users[update._id] = {};
        }
        state.users[update._id].presence = update.presence;
    },
    addUsers(state, update) {
        var users = Object.entries(update.data);

        //user [0] is user id, user[1] is json containing all user info
        users.forEach(user => {
            Vue.set(state.users, user[0], user[1]);
            if (!state.guilds[update.guild].members) {
            // Set guild.members equal to an empty array
            Vue.set(state.guilds[update.guild], 'members', []);
            }
            
            Vue.set(state.guilds[update.guild].members, user[0], state.users[user[0]]);
        });
    },
    userUpdate(state, update) {
        state.users[update.user._id] = update.user;

        var userExists = state.guilds[update.guild].members.some(member => {
            return member._id == update.user._id;
        });

        if (userExists) {
            state.guilds[update.guild].members[update.user._id] = update.user;
        } else {
            state.guilds[update.guild].members.push(state.users[update.user._id]);
        }

    }
};