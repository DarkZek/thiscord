class UserService {

    Initialize(appManager) {
        this.store = appManager.store;
        this.socket = appManager.socket;
        this.events = appManager.events;
        this.guildService = appManager.services.GuildService;

        this.SetupListeners();
    }

    SetupListeners() {
        //Listen to all incoming messages
        this.socket.on('presence-update', function(message) {
            window.store.commit('setPresence', message);
        }.bind(this));

        this.events.$on('join-guild', function(guild) { this.JoinGuild(guild); }.bind(this));

        this.events.$on(
            "leave-guild",
            function(data) {
                this.LeaveGuild(this.store.state.selectedGuild.guild._id);
            }.bind(this)
        );
    }

    CheckAuthentication(cb) {

        this.store = store;

        var token = $.cookie('token');

        if (token == null) {
            return cb(false);
        }

        $.GET("user/current", function(result, data) {
            if (result == "error") {
                cb(false, data.responseJSON);
            } else {
                cb(true, data.responseJSON);
            }
        });
    }

    GetUsers(guild, cb) {
        $.GET('guild/' + guild + "/users", function(status, data) {

            const currentUId = this.store.state.user._id;
            
            Object.entries(data.responseJSON).forEach(user => {
                if (user[1]._id == currentUId) {
                    data.responseJSON[user[1]._id].presence = 2;
                }
            });

            this.store.commit('addUsers', {
                guild: guild,
                data: data.responseJSON
            });
            cb();
        }.bind(this));
    }

    JoinGuild(guild) {
        $.POST('user/guilds/join', {
            invite: guild
        }, function(result, data) {
            if (result != 'success') {
                return window.log(data.error.message);
            }

            //Update guild
            this.guildService.LoadGuild(data.responseJSON.guild);
        }.bind(this));
    }

    LeaveGuild(guild) {
        $.POST(
            "user/guilds/leave",
            {
                "guild-id": guild
            },
            function(result, data) {
                if (result != "success") {
                    window.log(data);
                }

                this.store.commit("removeGuild", guild);

                if (Object.values(this.store.state.guilds).length == 0) {
                    //TODO: Make blank guilds work
                    window.location.reload();
                }
            }.bind(this)
        );
    }

    ChangeProfilePicture(guild) {
        //TODO:
    }
}

export default new UserService();