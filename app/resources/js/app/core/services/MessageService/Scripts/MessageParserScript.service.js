class CustomParseObject {
    constructor(Name, RegexStr, cb) {
        this.Name = Name;
        this.Regex = new RegExp(RegexStr);
        this.cb = cb;
    }
}

class ParseObject {
    constructor(Name, RegexStr, StartChar, EndChar) {
        this.Name = Name;
        this.Regex = new RegExp(RegexStr);
        this.Start = StartChar;
        this.End = EndChar;
    }
}

class MessageParserScript
{
    parseMessage(message)
    {

        // We need this to be in the order that we want as it will step through from start to end
        var parseObjects = [
            new ParseObject("Bold Italic", "(?:\\*\\*\\*)(.*?)(?:\\*\\*\\*)", "<em><strong>", "</strong></em>"),
            new ParseObject("Bold", "(?:\\*\\*)(.*?)(?:\\*\\*)", "<b>", "</b>"),
            new ParseObject("Italic", "(?:\\*)(.*?)(?:\\*)", "<em>", "</em>"),
            new ParseObject("Underline", "(?:__)(.*?)(?:__)", "<u>", "</u>"),
            new ParseObject("Italic2", "(?:_)(.*?)(?:_)", "<i>", "</i>"),
            new ParseObject("StrikeThrough", "(?:~~)(.*?)(?:~~)", "<strike>", "</strike>"),
            new ParseObject("Spoiler", "(?:\\|\\|)(.*?)(?:\\|\\|)", "<div class='spoiler'>", "</div>"),
        ];

        //Stop injections
        var NewMessage = message.message
            .replace(/&/g, "&amp;")
            .replace(/</g, "&lt;")
            .replace(/>/g, "&gt;")
            .replace(/"/g, "&quot;")
            .replace(/'/g, "&#039;");
        var TagCount = 0;

        parseObjects.forEach(function(Parso)
        {
            var RegexResult = NewMessage.split(Parso.Regex);
            if (RegexResult !== null && RegexResult.length > 1)
            {
                NewMessage = "";
                RegexResult.forEach(function(Str)
                {
                  // Counting while processing through this may be a bit much.
                  // Could probably be made into the for loop and itterate by 3s
                    TagCount += 1;
                    if (TagCount == 1 || TagCount == 3) {
                        NewMessage += Str;
                        if (TagCount >= 3) {
                          TagCount -= 3;
                        }
                    }
                    else if (TagCount == 2)
                    {
                        NewMessage += (Parso.Start + Str + Parso.End);
                    }
                });
            }
        });

        message.displayedMessage = "<span class=\"row\">" + NewMessage + "</span>";
        this.CheckForCustomMessage(message);
    }

    CheckForCustomMessage(message) {

        var customParsers = [
            new CustomParseObject('Invite Links', '(?:(?:https|http):\/\/thiscord.com\/i\/)([a-zA-Z-]*)', function(data, message) {

                if (data.length <= 1) {
                    return;
                }

                message.noBorder = true;
        
                //If loading whole channel then append to chanel, else add to current channel
                if (this.messages) {
                    this.messages.push({
                        type: 'Custom',
                        component: 'joinguild',
                        data: data[1]
                    });
                } else {
                    window.store.state.selectedGuild.channel.messages.push({
                        type: 'Custom',
                        component: 'joinguild',
                        data: data[1]
                    });
                }
            }.bind(this)),
            new CustomParseObject('HTTP Links', /(https?:\/\/(?:www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b(?:[-a-zA-Z0-9()@:%_\+.~#?&//=]*))/, function(data, message) {
                var resultMessage = "";

                if (data.length <= 1) {
                    return;
                }

                data.forEach(msg => {

                    if (!msg) {
                        return;
                    }

                    if (msg.match(/(https?:\/\/(?:www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b(?:[-a-zA-Z0-9()@:%_\+.~#?&//=]*))/)) {
                        resultMessage += "<a target=\"_blank\" href=\"" + msg + "\">" + msg + "</a>";
                    } else {
                        resultMessage += msg;
                    }
                });

                message.displayedMessage = resultMessage;
            }),
            new CustomParseObject("Mentions", /(?:@)([A-Za-z0-9]*)/, function(data, message) {

                if (data.length <= 1) {
                    return;
                }

                var resultMessage = "";

                //Loop over every 3 messages
                for (var i = 0; i < data.length; i += 3) {

                    var user = window.store.state.users[data[i + 1]];
                    var username = "";
                    if (user) {
                        username = user.name;
                    } else {
                        username = "Invalid User";
                    }

                    resultMessage += "<a class='mention' profile='" + data[i + 1] + "'>@" + username + "</a>";
                }

                message.displayedMessage = resultMessage;
            })
        ]

        customParsers.forEach(parser => {
            var result = message.displayedMessage.split(parser.Regex);

            parser.cb(result, message);
        });
    }

    //Parse many messages
    ParseMessages(messageList) {

        var messages = [];
        this.messages = messages;
        messageList.forEach(message => {
            message.type = 'Message';
            //Push message to list first so we can add custom messages after (like invite links)
            messages.push(message);
            this.parseMessage(message);
        });
        this.messages = null;

        return messages;
    }

    //Parse single message
    ParseMessage(message) {
        this.parseMessage(message);
    }
}

export default new MessageParserScript();