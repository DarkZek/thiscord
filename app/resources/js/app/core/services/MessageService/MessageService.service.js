import Parser from './Scripts/MessageParserScript.service.js';

class MessageService {

  Initialize(appManager) {
      this.store = appManager.store;
      this.socket = appManager.socket;
      this.SetupListeners();
  }

  GetMessages(guildId, channelId, cb) {

    $.GET('guild/' + guildId + "/" + channelId, function(result, data) {

        //Parse the chat
        var data = data.responseJSON;
        
        data = Parser.ParseMessages(data);

        store.commit('updateMessages', {
          guildId: guildId,
          channelId: channelId,
          messages: data
        });

        store.commit('setDisplayedMessages', {
          guildId: guildId,
          channelId: channelId
        });

        cb();
    });
  }
  
  SetupListeners() {
    //Listen to all incoming messages
    this.socket.on('chat-message', function(message) {
      Parser.ParseMessage(message);

      message.type = "Message";

      message.displayed = true;
      this.store.commit('addMessage', {
        message: message
      });
    }.bind(this));
  }
}

export default new MessageService();