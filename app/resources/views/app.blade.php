@extends('layout.head')

@section("head")
    <title>Thiscord</title>
@endsection

@section("content")
<body style="overflow: hidden; width: 100vw; height: 100vh;">
    <div class="blackout"></div>
    <div id="app" style="overflow: hidden; width: 100vw; height: 100vh;">
        <app></app>
    </div>
    <script src="{{ asset('js/global.js') }}"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    <script>
        //Let the client know who it is 
        <?php
        print("var api = '" . env("API_URL", "http://SET_API_URL_IN_ENV_FILE.com/") . "';");
        ?>

        var settings = {};
        settings.api = api;

        window.settings = settings;
        
    </script>
    <style>
        .blackout {
            transition: opacity 0.1s ease-in;
            width: 100vw;
            height: 100vh;
            position: absolute;
            background-color: rgba(10, 10, 10, 0.5);
            z-index: 3;
            display: none;
            opacity: 0;
            top: 0px;
        }
        .loader {
            position: absolute;
            top: 0px;
            left: 0px;
            width: 100vw;
            height: 100vh;
            background-color: #36393f;
            z-index: 4;
            animation: 2s loader;
            animation-fill-mode: forwards;
        }
        .rotater {
            animation: rotating 2s linear infinite;
            width: 40px;
            height: 40px;
            position: absolute;
            top: 40%;
            left: 0px;
            right: 0px;
            margin: auto;
            display: block;
        }

        .dot {
            border-radius: 10px;
            background-color: grey;
            height: 12px;
            width: 12px;
        }

        .dot1 {
            position: absolute;
            left: 0px;
            animation: 2s dots1 infinite;
            animation-delay: 1.25s;
        }
        .dot2 {
            position: absolute;
            right: 0px;
            animation: 2s dots2 infinite;
            animation-delay: 1.25s;
        }
        .dot3 {
            position: absolute;
            left: 0px;
            bottom: 0px;
            animation: 2s dots3 infinite;
            animation-delay: 0.25s;
        }
        .dot4 {
            position: absolute;
            right: 0px;
            bottom: 0px;
            animation: 2s dots4 infinite;
            animation-delay: 0.25s;
        }

        @keyframes rotating {
            to{ transform: rotate(360deg); }
        }

        @keyframes dots1 {
            25% {
                background-color: grey;
            }
            50% {
                background-color: red;
            }
            100%{
                background-color: grey;
            }
        }
        
        @keyframes dots2 {
            0%{
                background-color: grey;
            }
            25% {
                background-color: green;
            }
            50% {
                background-color: grey;
            }
        }
        @keyframes dots3 {
            0%{
                background-color: grey;
            }
            25% {
                background-color: #5454d0;
            }
            50% {
                background-color: grey;
            }
        }
        @keyframes dots4 {
            25% {
                background-color: grey;
            }
            50% {
                background-color: yellow;
            }
            75% {
                background-color: grey;
            }
        }

        .disableLoader {
            animation: 0.5s disableLoader ease-in;
            animation-fill-mode: forwards;
        }

        .disableLoader .rotater {
            display: none;
        }

        @keyframes disableLoader {
            0% {
                transform: scale(1);
            }
            99%{
                transform: scale(1.1);
                opacity: 0;
            }
            100% {
                visibility: hidden;
                opacity: 0;
            }
        }
    </style>
    <style id="guild-css">
    </style>
    <div class="loader">
        <div class="rotater">
            <div class="dot dot1"></div>
            <div class="dot dot2"></div>
            <div class="dot dot3"></div>
            <div class="dot dot4"></div>
        </div>
    </div>
</body>
@endsection