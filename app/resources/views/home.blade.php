@extends('layout.head')

@section("head")
    <script src="https://unpkg.com/vuex@3.1.1/dist/vuex.js" crossorigin="anonymous"></script>
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,700,700i,900,900i&display=swap" rel="stylesheet"> 
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.2.0/socket.io.dev.js"></script>
    <script src="https://cdn.jsdelivr.net/gh/google/code-prettify@master/loader/run_prettify.js?lang=css&amp;skin=sunburst"></script>
    <title>Thiscord - Text Chat for cool people!</title>

<style>
        .tg {
            border-collapse: collapse;
            border-spacing: 0;
            border: none;
            margin-top: 20px;
        }
    
        .tg td {
            font-family: Arial, sans-serif;
            font-size: 14px;
            padding: 10px 5px;
            border-style: solid;
            border-width: 0px;
            overflow: hidden;
            word-break: normal;
        }
    
        .tg th {
            font-family: Arial, sans-serif;
            font-size: 14px;
            font-weight: normal;
            padding: 10px 5px;
            border-style: solid;
            border-width: 0px;
            overflow: hidden;
            word-break: normal;
        }
    
        .tg .tg-Open {
            text-align: center;
            height: 45px;
        }
    
        .tg .tg-Down {
            text-align: center;
            height: 45px;
        }
    
        .btn-open {
            background-color: skyblue;
            border-radius: 12px;
            width: 100%;
            height: 100%;
        }
    
        .btn-download {
            border-radius: 12px;
            width: 100%;
            height: 100%;
        }
    
        body {
            background-color: #232427;
            margin: 0px;
            color: white;
        }
    
        #navIcon {
            max-height: 50px;
            padding-top: 0px;
            padding-bottom: 0px;
            height: 32px;
            width: 32px;
        }
    
        #navIconLink {
            /* Yeah, bad, blah blah, lazy #jpk */
            padding-top: 7px !important;
            padding-bottom: 5px !important;
            padding-right: 0px !important;
        }
    
        .JoinUs {
            margin-top: 28px;
        }
    
        #navSocial {
            padding-top: 7px !important;
            padding-bottom: 5px !important;
        }
    
        #ThePitch {
            max-width: 50%;
            margin: 0 auto;
    
            text-align: center;
            line-height: 24px;
        }
    
        #DownloadDiv {
            max-width: 30%;
            margin: 0 auto;
        }
    
        #JoinTheBois {
            max-width: 50%;
            margin: 0 auto;
            margin-top: 20px;
        }
    
        .Join {
            width: 100%;
            border-radius: 12px;
        }
    
        #footer {
            clear: both;
            color: lightgray;
            padding: 0;
            text-align: center;
            vertical-align: middle;
            line-height: normal;
            margin: 0;
            padding-top: 6px;
            margin-bottom: 4px;
            position: fixed;
            bottom: 0px;
            width: 100%;
        }

        .navbar-nav .nav-item > a {
            padding-top: 0px;
        }
    
    </style>
@endsection

@section("content")

<body>
    
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <a class="navbar-brand" href="#">Thiscord</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Download</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Devs
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="https://gitlab.com/darkzek/thiscord">Gitlab</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="https://gitlab.com/darkzek">DarkZek</a>
                    <a class="dropdown-item" href="https://simplyjpk.com">SimplyJPK</a>
                    </div>
                </li>
                </ul>
                <form class="form-inline my-2 my-lg-0">
                    <li class="nav-item">
                        <a class="nav-link" href="/app">Open</a>
                    </li>
                </form>
            </div>
        </nav>
        <!-- End Menu Junk-->
        <br>
        <br>
        <!-- Give them the good pitch, not the bad one. -->
        <div id='ThePitch'>
            <h2>Sick of everyone having their own communication platform?</h2>
            <h3>So are we!</h3>
            <p class='JoinUs'>Join us on <b>Thiscord</b>, the platform made for gamers for 'free' which might be secure, and
                should maybe
                work on both desktop and phone! Stop accepting other platforms for their rightful superiority, support
                <b>Thiscord</b>!</p>
        </div>
        <!-- Maybe it wasn't so good -->
        <!-- Let 'em download or Open -->
        <div id='DownloadDiv' style="container">
            <th class="tg-Open"><input type="button" class='btn-open'
                    onclick="location.href='/app';" value="Open Thiscord" /></th>
            <th></th>
        </div>
        <!-- End Download or Open -->
        
        <!-- Join Us -->
        <!-- End the joining -->
    </body>
    <footer>
        <div class="footer" id="footer">Copyright © 2019 <a href='http://60.234.120.113'>Thiscord</a>. All Rights Reserved.
        </div>
    </footer>
    
@endsection