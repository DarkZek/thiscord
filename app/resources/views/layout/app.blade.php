@extends('layout.head')

@section("head")
    <title>ThisCord</title>
@endsection

@section("content")
<body>
    @yield('content')
    <script src="{{ asset('js/global.js') }}"></script>
</body>
@endsection