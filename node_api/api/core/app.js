const express = require('express');
const app = express();
const morgan = require('morgan');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const auth = require('./Services/AuthService');

const uploadRoutes = require('../modules/Upload/UploadRouter');
const guildRoutes = require('../modules/Guild/GuildRouter');
const channelRoutes = require('../modules/Channel/ChannelRouter');
const userRoutes = require('../modules/User/UserRouter');
const messageRoutes = require('../modules/Message/MessageRouter');

const {
    MONGO_USERNAME,
    MONGO_PASSWORD,
    MONGO_HOSTNAME,
    MONGO_PORT,
    MONGO_DB
  } = process.env;

const options = {
    useNewUrlParser: true,
    reconnectTries: Number.MAX_VALUE,
    reconnectInterval: 500, 
    connectTimeoutMS: 10000,
};

var connected = false;

while (!connected) {
    console.log("Trying connection...");
    connected = true;
    mongoose.connect(`mongodb://${MONGO_USERNAME}:${MONGO_PASSWORD}@${MONGO_HOSTNAME}:${MONGO_PORT}/${MONGO_DB}?authSource=admin`, options).catch( function(err) {
        connected = false;
    });

    var waitTill = new Date(new Date().getTime() + 1000);
    while(waitTill > new Date()){}
}

console.log("Connected! Now servicing users..");

app.use((req, res, next) => {

    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader("Access-Control-Allow-Credentials", "true");
    res.setHeader("Access-Control-Allow-Headers", "Authorization, x-socket-id, Access-Control-Allow-Headers, Origin, Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");

    if (req.method === 'OPTIONS') {
        res.setHeader("Access-Control-Allow-Methods", "GET, PATCH, HEAD, OPTIONS, POST, PUT, DELETE");
        return res.status(200).json({}).end();
    }

    //Add custom error handler
    res.error = function(err, message, code) {

        if (!err) {
            err = {};
        }

        if (code != 404) {
            console.log(err);
        }

        if (message) {
            err.message = message;
        }
        
        if (!code) {
            code = 400;
        }

        if (!res.headersSent) {
            res.status(code).json({ error: { message: err.message }});
        }

    };

    next();
});

app.use(uploadRoutes);
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(morgan('dev'))

//Listen for routes
app.get('/', (req, res, next) => {
    res.status(301).redirect('https://gitlab.com/DarkZek/thiscord/blob/master/API%20Specification.md');   
});
app.use('/guild', auth, guildRoutes);
app.use('/guild', auth, messageRoutes);
app.use('/guild', auth, channelRoutes);
app.use('/user/', userRoutes);

app.use((req, res, next) => {
    const error = new Error('Not Found');
    error.status = 404;
    next(error);
});

app.use((error, req, res, next) => {
    try {
        res.status(error.status || 500).json({
            error: {
                message: error.message
            }
        });
    } catch(err) {}
    if (error.status == 404) {return;}
    console.log(error);
});

module.exports = app;