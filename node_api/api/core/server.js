const http = require('http');
const app = require('./app');
const WebsocketService = require("./Services/WebsocketService");
const port = process.env.PORT || 80;
const server = http.createServer(app);

server.listen(port);

WebsocketService.Setup(server);
app.sockets = WebsocketService;

const readline = require('readline');
var rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
  terminal: false
});

rl.on('line', (str) => {
  switch(str) {
    case 'online': {
      console.log("There are " + Object.keys(WebsocketService.users).length + " users online");
      break;
    }
    case 'help': {
        console.log("--== Thiscord Help ==--");
        console.log("online - Tells you the number");
        console.log("of currently online users.");
        break;
    }
    default: {
        console.log("Invalid command, use help for help");
        break;
    }
    console.log();
  }
});