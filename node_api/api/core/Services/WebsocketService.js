const jwt = require('jsonwebtoken');
const mongoose = require('mongoose');
const User = require('../../modules/User/Models/user');
const Channel = require('../../modules/Channel/Models/channel');

class Websockets {

    Setup(server) {

        //Setup variables
        this.channels = [];
        var channels = this.channels;

        this.guilds = [];

        this.users = [];
        var users = this.users;

        const socket = require('socket.io');
        var io = socket(server);

        io.on('connection', function(socket) {
            
            socket.on('auth', function(data) {
                try {
                    const decoded = jwt.verify(data.token, process.env.JWT_KEY);
            
                    const id = decoded.id;

                    //Create a count of users logged into account
                    if (users[id] == null) {
                        users[id] = [socket];
                    } else {
                        users[id].push(socket);
                    }

                    //Allow access to all routes
                    this.CreateRoutes(socket, channels, users, id);

                    this.UpdatePresence(id, 2, socket);
                    
                } catch (error) {
                    console.log(error);
                    socket.disconnect();
                }
            }.bind(this));
        }.bind(this));
    }

    UpdatePresence(id, pr, socket) {
        //Send out user online presence updates
        User.findById(id).populate('guilds').then(user => {

            user.guilds.forEach(guild => {
                //Make sure array exists
                if (!this.guilds[guild._id]) {
                    this.guilds[guild._id] = [];
                }

                //Send presence update
                Object.values(this.guilds[guild._id]).forEach(client => {
                    client.emit('presence-update', { _id: user._id, presence: pr });
                });

                this.guilds[guild._id][user._id] = socket;
            });

        }).catch(err => {
            console.log("Error connecting to database. " + err);
        });
    }

    RemovePresence(id, pr, socket) {
        //Send out user online presence updates
        User.findById(id).populate('guilds').then(user => {

            user.guilds.forEach(guild => {
                //Make sure array exists
                if (!this.guilds[guild._id]) {
                    this.guilds[guild._id] = [];
                }

                //Send presence update
                Object.values(this.guilds[guild._id]).forEach(client => {
                    client.emit('presence-update', { _id: user._id, presence: pr });
                });

                var index = this.guilds[guild._id].indexOf(socket);
                this.guilds[guild._id].splice(index);
            });

        }).catch(err => {
            console.log("Error connecting to database. " + err);
        });
    }

    RemoveUserFromChannel(channels, channel, socket) {
        for (var i = 0; i < channels[channel].length; i++) {
            if (channels[channel][i] == socket) {
                channels[channel].splice(i);
                return;
            }
        }
    }
    CreateRoutes(socket, channels, users, id) {
        var listeningChannel = "";

        socket.on('join', function(data) {

            Channel.findById(data.channel)
            .populate('guild').then(channel => {

                if (!channel) {
                    //Request rejected
                    return;
                }
                
                //If channel doesnt exist
                if (this.guilds[channel.guild._id] == null) {
                    this.guilds[channel.guild._id] = [];
                }

                if (channels[data.channel] == null) {
                    channels[data.channel] = [];
                }

                //TODO: Verify they have access to the channel

                if (listeningChannel != "") {
                    //They are switching channels
                    this.RemoveUserFromChannel(channels, listeningChannel, socket);
                }

                channels[data.channel].push(socket);
                listeningChannel = data.channel;
            });
        }.bind(this));

        socket.on('disconnect', function() {
            console.log('Got disconnect!');
      
            if (listeningChannel != "") {
                this.RemoveUserFromChannel(channels, listeningChannel, socket);
            }

            //Create a count of users logged into account
            if (users[id].length == 1) {
                users[id] = null;
                this.RemovePresence(id, 0);
            } else {
                for (var i = 0; i < users[id].length; i++) {
                    if (users[id][i] == socket) {
                        users[id].splice(i);
                        return;
                    }
                }
            }
         }.bind(this));
    }
}

module.exports = new Websockets();