const express = require('express');
const router = express.Router();
const GuildService = require('./Services/GuildService');
const GuildImages = require('./Services/GuildImages');
const GuildInfo = require('./Services/GuildInfo');
const multer = require('multer');

const fileFilter = (req, file, cb) => {
    if (file.mimetype === 'image/jpeg' || 
        file.mimetype === 'image/png' || 
        file.mimetype === 'image/gif') {
        cb (null, true);
    } else {
        cb (null, false);
    }
};

const storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, './uploads/guilds/')
    },
    filename: function(req, file, cb) {
        cb(null, "tmp" + new Date().toISOString() + "." + file.originalname.split('.').pop())
    }
});

const upload = multer({storage: storage,
    limits: {
        //10mb
        fileSize: 1024 * 1024 * 10,
    },
    fileFilter: fileFilter
});

router.post('/create', upload.single('icon'), (req, res, next) => GuildService.Create(req, res));

router.get('/invite',  (req, res, next) => GuildInfo.GetInviteInfo(req, res));

router.get('/:guildId', (req, res, next) => GuildInfo.GetInfo(req, res));

router.get('/:guildId/users', (req, res, next) => GuildInfo.GetMembers(req, res));

router.patch('/:guildId', (req, res, next) => GuildService.UpdateGuild(req, res));

router.patch('/:guildId/icon', upload.single('icon'), (req, res, next) => GuildImages.UpdateIcon(req, res));

router.patch('/:guildId/background', upload.single('icon'), (req, res, next) => GuildImages.UpdateBackground(req, res));

router.delete('/:guildId', (req, res, next) => GuildService.DeleteGuild(req, res));

module.exports = router;