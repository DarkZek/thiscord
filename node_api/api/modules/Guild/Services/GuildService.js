var fs = require('fs');
const ChannelController = require('../../Channel/Services/ChannelChanges');
const GuildService = require('../../User/Services/GuildService');
const Guild = require('../Models/guild');
const mongoose = require('mongoose');

exports.Create = function(req, res) {

    const guild = new Guild({
        _id: new mongoose.Types.ObjectId(),
        name: req.body.name,
        css: req.body.css,
        settings: {
            invite: Math.random().toString(36).substring(3)
        },
        owner_id: req.user.id,
        created: new Date()
    });

    guild.save().then(result => {

        if(req.file) {
            //Move the file to be SERVER_ID.extension
            fs.rename(req.file.path, req.file.destination + result._id + ".jpg", (err) => {
                if (err) throw err;
                console.log('Rename complete!');
            });
        }

        //Create default channel
        ChannelController.AddChannel({
            body: {
                css: "",
                name: "chat",
                topic: "Your new chat room"
            },
            params: {
                guildId: result._id
            },
            user: req.user
        });

        GuildService.ForceJoinGuild({
            user: req.user,
            body: {
                guildId: result._id
            }
        });

        res.json({
            message: "Success",
            guild: {
                _id: result._id,
                name: result.name,
                css: result.css,
                settings: result.settings,
                owner_id: result.owner_id,
                created: result.created
            }
        });

        console.log("New guild created, welcome " + req.body.name);
        
    }).catch(err => res.error(err));
};

exports.UpdateGuild = function(req, res) {

    //TODO: Require authentication
    
    const changes = { };

    //Only allow name, css and settings to be changed
    console.log(req.body);
    if (req.body['brand[name]']) {
        changes.name = req.body['brand[name]'];
    }

    if (req.body['brand[invite]']) {
        changes['settings.invite'] = req.body['brand[invite]'];
    }

    if (req.body.css != null) {
        changes.css = req.body.css;
    }

    Guild.findOneAndUpdate({ _id: req.params.guildId }, changes).exec().then(result => {
        if (result) {
            res.status(200).json({ message: "Success" });
        } else {
            return res.status(401).json({ error: 'Auth failed' });
        }
    }).catch(err => res.error(err));

};

exports.DeleteGuild = function(req, res) {
    const id = req.params.guildId;
    Guild.findOneAndRemove({ _id: req.params.messageId, owner_id: req.user.id }).exec().then(result => {
        if (result) {
            res.status(200).json({ message: "Successfully deleted object" });
        } else {
            return res.status(401).json({ error: 'Auth failed' });
        }
    }).catch(err => res.error(err));
};