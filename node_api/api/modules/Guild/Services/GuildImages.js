var fs = require('fs');

exports.UpdateBackground = function(req, res) {

    //TODO: Check permission

    if (!req.file) {
        return res.status(400).json({error: { message: "No image provided" }})
    }

    //Move the file to be SERVER_ID.extension
    fs.rename(req.file.path, "./uploads/guilds/backgrounds/" + req.params.guildId + ".jpg", (err) => {
        if (err) throw err;
        res.status(200).json({ message: "Success" });
    });

};

exports.UpdateIcon = function(req, res) {

    //TODO: Check permission
    if (!req.file) {
        return res.status(400).json({error: { message: "No image provided" }})
    }

    //Move the file to be SERVER_ID.extension
    fs.rename(req.file.path, req.file.destination + req.params.guildId + ".jpg", (err) => {
        if (err) throw err;
        res.status(200).json({ message: "Success" });
    });
};