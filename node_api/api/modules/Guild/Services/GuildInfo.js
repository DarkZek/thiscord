const Channel = require('../../Channel/Models/channel');
const Guild = require('../Models/guild');
const User = require('../../User/Models/user');
const mongoose = require('mongoose');

exports.GetInfo = function(req, res) {
    const id = req.params.guildId;

    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({ error: { message: "Invalid guild id" } });
    }

    Guild.findById(id).exec().then(guild => {

        if (guild) {
            //Get channels
            Channel.find({ guild: id }).exec().then(channel => {

                //Get user count
                User.find({ guilds: id }).count().exec(function (err, userCount) {

                    var onlineMembers = 0;

                    if (req.app.sockets.guilds[id]) {
                        onlineMembers = req.app.sockets.guilds[id].length;
                    }

                    const response = {
                        _id: guild.id,
                        name: guild.name,
                        settings: guild.settings,
                        owner_id: guild.owner_id,
                        created: guild.created,
                        css: guild.css,
                        invite: guild.settings.invite,
                        channels: {},
                        memberCount: userCount,
                        onlineCount: onlineMembers
                    };
    
                    channel.forEach((element, index) => {
                        response.channels[element._id] = {
                            _id: element._id,
                            name: element.name,
                            created: element.created,
                            topic: element.topic,
                            css: element.css
                        };
                    });
    
                    res.status(200).json(response);
                });

            }).catch(err => res.error(err));
            
        } else {
            res.status(404).json({ error: { message: "No guild found" }});
        }
        
    }).catch(err => res.error(err, 'Invalid guild ID'));
};

exports.GetMembers = function(req, res) {
    const id = req.params.guildId;

    User.find({ guilds: id }).exec().then(users => {

        var userMap = {};

        users.forEach(function(user) {
            userMap[user._id] = {
                _id: user._id,
                name: user.name,
                identifier: user.identifier,
                presence: req.app.sockets.users[user._id] ? 2 : 0
            };
        });

        res.status(200).json(userMap);  
        
    }).catch(err => res.error(err));
};

exports.GetInviteInfo = function(req, res) {

    if (!req.query.invite) {
        return res.status(400).json({ error: 'No invite sent' })
    }

    const id = req.params.guildId;

    Guild.findOne({ 'settings.invite': req.query.invite }).exec().then(result => {

        if (!result) {
            return res.status(400).json({ error: 'Invalid invite' })
        }

        //Get user count
        User.find({ guilds: result._id }).count().exec(function (err, userCount) {

            var onlineMembers = 0;

            if (req.app.sockets.guilds[id]) {
                onlineMembers = req.app.sockets.guilds[id].length;
            }

            return res.status(200).json({guild: {
                _id: result._id,
                members: userCount,
                online: onlineMembers,
                name: result.name
            }});

        });

    }).catch(err => res.error(err));

}