const mongoose = require('mongoose');
const Message = require('../Models/message');

exports.GetChannel = function(req, res) {
    Message.find({channel_id: req.params.channelId}).sort('-sent').limit(20).exec()
        .then(result => {
            res.status(200).json(result.reverse());
        }).catch(err => res.error(err));
};

exports.Send = function(req, res) {
    const message = new Message({
        _id: new mongoose.Types.ObjectId(),
        message: req.body.message,
        originalMessage: req.body.message,
        sender_id: req.user.id,
        guild_id: req.params.guildId,
        channel_id: req.params.channelId,
        group: 0,
        sent: new Date()
    });

    message.save().then(result => {
        
        //Send live update
        if (req.app.sockets.channels[req.params.channelId]) {
            req.app.sockets.channels[req.params.channelId].forEach(client => {
                client.emit('chat-message', message);
            });
        }

        res.json({
            message: "Success",
        });
        
    }).catch(err => res.error(err));
};

exports.GetMessage = function(req, res) {
    const id = req.params.guildId;

    Message.findById(id).exec().then(message => {

        if (message) {
            res.status(200).json(message);
            
        } else {
            res.status(404).json({error: { message: "No guild found" }});
        }
        
    }).catch(err => res.error(err));
};

exports.EditMessage = function(req, res) {

    const changes = {};

    //Only allow name, css and settings to be changed
    if (req.body.message) {
        changes.message = req.body.message;
    }
    
    Message.findOneAndUpdate({ _id: req.params.messageId, owner_id: req.user.id }, changes).exec().then(result => {

        if (result) {
            res.status(200).json({ message: "Success" });
        } else {
            return res.status(401).json({ error: 'Auth failed' });
        }
    }).catch(err => res.error(err));

};

exports.DeleteMessage = function(req, res) {
    Message.findOneAndDelete({ _id: req.params.messageId, sender_id: req.user.id }).exec().then(result => {
        if (result) {
            res.status(200).json({ message: "Successfully deleted object" });
        } else {
            return res.status(401).json({ error: 'Auth failed' });
        }
    }).catch(err => res.error(err));
};