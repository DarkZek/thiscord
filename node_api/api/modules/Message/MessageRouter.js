const express = require('express');
const router = express.Router();
const MessageService = require('./Services/MessageService');

router.get('/:guildId/:channelId/', (req, res, next) => MessageService.GetChannel(req, res));

router.post('/:guildId/:channelId/message/send', (req, res, next) => MessageService.Send(req, res));

router.get('/:guildId/:channelId/:messageId', (req, res, next) => MessageService.GetMessage(req, res));

router.patch('/:guildId/:channelId/:messageId', (req, res, next) => MessageService.EditMessage(req, res));

router.delete('/:guildId/:channelId/:messageId', (req, res, next) => MessageService.DeleteMessage(req, res));

module.exports = router;