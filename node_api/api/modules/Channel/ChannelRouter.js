const express = require('express');
const router = express.Router();
const ChannelChanges = require('./Services/ChannelChanges');
const ChannelInfo = require('./Services/ChannelInfo');
const ChannelTyping = require('./Services/ChannelTyping');

router.get('/:guildId/list', (req, res, next) => ChannelInfo.ListChannels(req, res));

router.post('/:guildId/create', (req, res, next) => ChannelChanges.AddChannel(req, res));

router.patch('/:guildId/:channelId', (req, res, next) => ChannelChanges.UpdateChannel(req, res));

router.delete('/:guildId/:channelId', (req, res, next) => ChannelChanges.DeleteChannel(req, res));

router.post('/:guildId/:channelId/type', (req, res, next) => ChannelTyping.StartTyping(req, res));

module.exports = router;