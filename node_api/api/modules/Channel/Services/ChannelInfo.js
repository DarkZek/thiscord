const mongoose = require('mongoose');
const Channel = require('../../Channel/Models/channel');

exports.ListChannels = function(req, res) {
    Channel.find({ guild: req.params.guildId }).exec().then(channel => {
        const response = [];

        channel.forEach((element, index) => {
            response[index] = {
                _id: element._id,
                name: element.name,
                created: element.created,
                topic: element.topic,
                css: element.css
            };
        });

        res.status(200).json(response);
    }).catch(err => req.error(err));
}