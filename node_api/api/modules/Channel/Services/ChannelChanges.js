const Guild = require('../../Guild/Models/guild');
const Channel = require('../Models/channel');
const mongoose = require('mongoose');

exports.AddChannel = function(req, res) {

    Guild.findById(req.params.guildId, function(err, guild) {

        if (guild.owner_id != req.user.id) {
            if (res) {
                res.status(401).json({ error: { message: "You're not authorized to do that" }, user: user});
            }
            return;
        }

        const channel = new Channel({
            _id: mongoose.Types.ObjectId(),
            name: req.body.name,
            created: new Date(),
            topic: req.body.topic,
            css: req.body.css,
            guild: req.params.guildId
        });
    
        channel.save().then(result => {
            if (res) {
                res.status(201).json({
                    message: "Success",
                    channel: {
                        _id: result._id,
                        css: result.css,
                        name: result.name,
                        created: result.created,
                        topic: result.topic,
                        guild: result.guild
                    }
                });
            }
        }).catch(err => res.error(err));
    });
};

exports.UpdateChannel = function(req, res) {
    const changes = {};

    //Only allow name, css and settings to be changed
    if (req.body.name) {
        changes.name = req.body.name;
    }

    if (req.body.css) {
        changes.css = req.body.css;
    }

    if (req.body.topic) {
        changes.topic = req.body.topic;
    }
    
    Channel.findByIdAndUpdate(req.params.channelId, changes).exec().then(result => {
        if (result) {
            res.status(200).json({
                _id: result._id,
                css: result.css,
                name: result.name,
                created: result.created,
                topic: result.topic
            });
        } else {
            res.status(404).json({
                message: "No channel found"
            });
        }
    }).catch(err => req.error(err));
};

exports.DeleteChannel = function(req, res) {
    res.status(200).json({
        message: "Handling DELETE requests to /channel/" + req.params.guildId + "/" + req.params.channelId,
    });
};