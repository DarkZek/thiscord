exports.StartTyping = function(req, res) {

    if (!req.app.sockets.channels[req.params.channelId]) {
        res.status(400).json({error: {message: "Invalid channel"}});
        return;
    }

    //Send live update
    req.app.sockets.channels[req.params.channelId].forEach(client => {
        client.emit('chat-typing', {
            user: req.user.id
        });
    });
    
    res.status(200).json({message: "Success"});
};