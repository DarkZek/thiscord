const express = require('express');
const router = express.Router();
var fs = require('fs');

router.get('/guild/:guildId/icon', (req, res, next) => {

    const extensions = ["png", "jpg", "jpeg", "gif"];

    for (var i = 0; i < extensions.length; i++) {
        const path = 'uploads/guilds/' + req.params.guildId + "." + extensions[i];

        if (fs.existsSync(path)) {
            res.sendFile(path, { root : process.cwd()});
            return;
        }
    }

    res.sendFile('uploads/guilds/default.png', { root : process.cwd()});
});

router.get('/guild/:guildId/background', (req, res, next) => {

    const extensions = ["png", "jpg", "jpeg", "gif"];

    for (var i = 0; i < extensions.length; i++) {
        const path = 'uploads/guilds/backgrounds/' + req.params.guildId + "." + extensions[i];

        if (fs.existsSync(path)) {
            res.sendFile(path, { root : process.cwd()});
            return;
        }
    }

    res.sendFile('uploads/guilds/backgrounds/default.png', { root : process.cwd()});
});

router.get('/user/:userId/profile', (req, res, next) => {

    const extensions = ["png", "jpg", "jpeg", "gif"];

    for (var i = 0; i < extensions.length; i++) {
        const path = 'uploads/users/' + req.params.userId + "." + extensions[i];

        if (fs.existsSync(path)) {
            res.sendFile(path, { root : process.cwd()});
            return;
        }
    }

    res.sendFile('uploads/users/default.jpeg', { root : process.cwd()});
});

module.exports = router;