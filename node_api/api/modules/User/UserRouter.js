const express = require('express');
const router = express.Router();
const multer = require('multer');
const auth = require('../../core/Services/AuthService');
const GuildService = require('./Services/GuildService');
const UserService = require('./Services/UserService');
const AuthService = require('./Services/AuthService');
const ProfileService = require('./Services/ProfileService');

const fileFilter = (req, file, cb) => {
    if (file.mimetype === 'image/jpeg' || 
        file.mimetype === 'image/png' || 
        file.mimetype === 'image/gif') {
        cb (null, true);
    } else {
        cb (null, false);
    }
};

const storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, './uploads/users/')
    },
    filename: function(req, file, cb) {
        cb(null, "tmp" + new Date().toISOString() + "." + file.originalname.split('.').pop())
    }
});

const upload = multer({storage: storage,
    limits: {
        //10mb
        fileSize: 1024 * 1024 * 10,
    },
    fileFilter: fileFilter
});

router.post('/register', upload.single('icon'), (req, res, next) => AuthService.Register(req, res));

router.post('/login', (req, res, next) => AuthService.Login(req, res));

router.delete('/:userId/', auth, (req, res, next) => UserService.DeleteUser(req, res));

router.get('/current', auth, (req, res, next) => UserService.GetCurrentUser(req, res));

router.patch('/:userId/profile', auth, upload.single('icon'),  (req, res, next) => ProfileService.UpdatePicture(req, res));

router.post('/guilds/join', auth, (req, res, next) => GuildService.JoinGuild(req, res));

router.post('/guilds/leave', auth, (req, res, next) => GuildService.LeaveGuild(req, res));

router.get('/guilds/list', auth, (req, res, next) => GuildService.ListGuilds(req, res));

router.get('/:userId', auth, (req, res, next) => UserService.GetUser(req, res));

module.exports = router;