const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const fs = require('fs');
const mongoose = require('mongoose');
const User = require('../Models/user');

exports.Login = function(req, res) {
    User.findOne({ email: req.body.email }).exec().then(user => {
        if (!user) {
            return res.error(null, 'Auth Failed', 400)
        }

        bcrypt.compare(req.body.password, user.password, (err, same) => {
            if (err) {
                return res.error(err, 'Auth Failed', 400)
            }

            if (same) {
                //Create signed token
                const token = jwt.sign({
                    id: user.id,
                    name: user.name,
                    email: user.email,
                    identifier: user.identifier,
                }, process.env.JWT_KEY, {
                    expiresIn: "1 day"
                });

                return res.status(200).json({
                    message: 'Auth successful',
                    token: token
                });
            } else {
                return res.status(400).json({
                    error: 'Auth failed'
                });
            }
        });

    }).catch(err => res.error(err));
};

exports.Register = function(req, res) {

    if (req.body.password.length > 50 || req.body.password.length < 2) {
        return res.error(err, 'Auth Failed', 400)
    }

    User.findOne({ email: req.body.email }).exec().then(user => {
        if (user) {
            return res.error(null, 'Email already in use', 409);
        } else {
            bcrypt.hash(req.body.password, 10, (err, hash) => {
                if (err) {
                    return res.status(500).json({
                        error: err.message
                    });
                } else {
                    const user = new User({
                        _id: new mongoose.Types.ObjectId(),
                        name: req.body.name,
                        email: req.body.email,
                        password: hash,
                        created: new Date()
                    });
        
                    user.save().then(result => {
                        if (req.file) {
                            //Move the file to be SERVER_ID.extension
                            try {
                                fs.rename(req.file.path, req.file.destination + result._id + ".jpg", (err) => { console.log(err); })
                            } catch (err) {
                                console.log(err);
                            }
                        }

                        res.status(201).json({
                            message: "Success"
                        });

                    }).catch(err => res.error(err));
                }
            });
        }
    }).catch(err => res.error(err));
};