const express = require('express');
const router = express.Router();
var fs = require('fs');

exports.UpdatePicture = function(req, res) {

    //TODO: Check permission

    if (!req.file) {
        return res.status(400).json({error: { message: "No image provided" }})
    }

    //Move the file to be SERVER_ID.extension
    fs.rename(req.file.path, "./uploads/users/" + req.user.id + ".jpg", (err) => {
        if (err) throw err;
        res.status(200).json({ message: "Success" });
    });

};