const User = require("../../User/Models/user");
const mongoose = require("mongoose");
const Guild = require("../../Guild/Models/guild");

exports.JoinGuild = function(req, res) {
  //Find guild
  Guild.findOne({ "settings.invite": req.body.invite })
    .then(guild => {
      if (!guild) {
        return res.status(404).json({ error: "Invite link not valid" });
      }

      User.findById(req.user.id)
        .populate("guilds")
        .then(user => {
          var alreadyInGuild = user.guilds.some(function(g) {
            return g.equals(guild._id);
          });

          if (alreadyInGuild) {
            return res
              .status(400)
              .json({ error: { message: "User is already in guild" } });
          }

          user.guilds.push(guild._id);
          user.save();
          res.status(200).json({ message: "Success", guild: guild._id });

          //Notify all guild members
          Object.values(req.app.sockets.guilds[guild._id]).forEach(client => {
            client.emit('user-update', { user: {
              _id: user.id,
              name: user.name,
              identifier: user.identifier,
              email: user.email,
              created: user.created
            }, guild: guild._id});
          });
        })
        .catch(err => res.error(err, null, 400));
    })
    .catch(err => res.error(err));

  return;
};

exports.ForceJoinGuild = function(req, res) {
  User.findById(req.user.id)
    .populate("guilds")
    .then(result => {
      if (!result) {
        if (res)
          res.status(404).json({ error: { message: "User doesn't exist" } });
        return;
      }

      if (result.guilds.includes(mongoose.Types.ObjectId(req.body.guildId))) {
        res
          .status(400)
          .json({ error: { message: "User is already in guild" } });
        return;
      }

      result.guilds.push(req.body.guildId);
      result.save();

      if (res) {
        res.status(200).json({ message: "Success" });
      }
    })
    .catch(err => {
      if (res) {
        res.error(err, null, 404);
      } else {
        console.log(err);
      }
    });
};

exports.ListGuilds = function(req, res) {
  User.findById(req.user.id)
    .populate("guilds")
    .then(result => {
      var guilds = [];

      result.guilds.forEach(guild => {
        guilds.push({
          _id: guild._id,
          name: guild.name,
          settings: guild.settings,
          owner_id: guild.owner_id,
          created: guild.created,
          css: guild.css
        });
      });

      res.status(200).json(guilds);
    })
    .catch(err => res.error(err, null, 404));
};

exports.LeaveGuild = function(req, res) {
  //Find guild
  Guild.findOne({ _id: req.body["guild-id"] })
    .then(guild => {
      if (!guild) {
        return res
          .status(400)
          .json({ error: "ID not valid" + req.body["guild-id"] });
      }

      User.findById(req.user.id)
        .populate("guilds")
        .then(user => {
          var alreadyInGuild = user.guilds.some(function(g) {
            return g.equals(guild._id);
          });

          if (!alreadyInGuild) {
            return res
              .status(400)
              .json({ error: { message: "User not in guild" } });
          }

          if (guild.owner_id == user._id) {
            return res
              .status(400)
              .json({
                error: { message: "Cant leave server when you're the owner" }
              });
          }

          for (var i = 0; i < user.guilds.length; i++) {
            if ((user.guilds[i]._id = guild._id)) {
              user.guilds.splice(i, 1);
              break;
            }
          }

          user.save();
        })
        .catch(err => res.error(err, null, 500));

      res.status(200).json({ message: "Success" });
    })
    .catch(err => res.error(err));

  return;
};
