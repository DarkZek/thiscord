const User = require('../Models/user');

exports.DeleteUser = function(req, res) {
    if (req.params.userId !== req.user.id) {
        return res.status(400).json({ error: 'Auth failed' });
    }

    User.findByIdAndDelete(req.params.userId).exec().then(result => {
        res.status(200).json({message: "Success"});
    }).catch(err => res.error(err));
};

exports.GetUser = function(req, res) {
    User.findById(req.params.userId).exec().then(result => {
        if (result) {
            res.status(200).json({
                _id: result.id,
                name: result.name,
                identifier: result.identifier,
                created: result.created
            });
        } else {
            res.status(404).json({message: "User not found"});
        }
    }).catch(err => res.error(err));
};

exports.GetCurrentUser = function(req, res) {
    User.findById(req.user.id).exec().then(result => {
        if (result) {
            res.status(200).json({
                _id: result.id,
                name: result.name,
                identifier: result.identifier,
                email: result.email,
                created: result.created
            });
        } else {
            res.status(404).json({message: "User not found"});
        }
    }).catch(err => res.error(err));
};